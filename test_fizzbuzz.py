import pytest
from fizzbuzz import FizzBuzz

class Test_class:

    @pytest.fixture(scope="function",autouse=True)
    def setup(self):
        print("\nsetup function")
        yield
        print("\nteardown function")

    def test_that_number_is_divisible_by_three(self):
        obj=FizzBuzz()
        assert obj.is_divisible_by(3, 3) == True

    def test_that_number_is_NOT_divisible_by_three(self):
        obj=FizzBuzz()
        assert obj.is_divisible_by(1, 3) == False

    def test_that_number_is_divisible_by_five(self):
        obj=FizzBuzz()
        assert obj.is_divisible_by(5, 5) == True

    def test_that_number_is_NOT_divisible_by_five(self):
        obj=FizzBuzz()
        assert obj.is_divisible_by(1, 5) == False

    def test_that_number_is_divisible_by_three_and_five(self):
        obj=FizzBuzz()
        assert obj.is_divisible_by(15, 15) == True

    def test_that_number_is_NOT_divisible_by_three_and_five(self):
        obj=FizzBuzz()
        assert obj.is_divisible_by(1, 15) == False

    def test_says_fizz(self):
        obj=FizzBuzz()
        assert obj.says(3) == "fizz"

    def test_says_buzz(self):
        obj=FizzBuzz()
        assert obj.says(5) == "buzz"

    def test_says_fizzbuzz(self):
        obj=FizzBuzz()
        assert obj.says(30) == "fizzbuzz"

    def test_says_nothing(self):
        obj=FizzBuzz()
        assert obj.says(1) == 1

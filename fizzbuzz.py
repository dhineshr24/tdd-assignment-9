class FizzBuzz:

    def is_divisible_by(self, number, divisor):
        return number % divisor == 0

    def says(self,number):
        if self.is_divisible_by( number, 15):
            return "fizzbuzz"
        elif self.is_divisible_by(number, 3):
            return "fizz"
        elif self.is_divisible_by(number, 5):
            return "buzz"
        else:
            return number

if __name__== "__main__":
    obj=FizzBuzz()
    for i in range(1,101):
        print(obj.says(i))